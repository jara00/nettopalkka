<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <?php
        $brutto = filter_input(INPUT_POST,'brutto',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $ennakko = filter_input(INPUT_POST,'ennakko',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tyoelake = filter_input(INPUT_POST,'tyoelake',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $vakuutus = filter_input(INPUT_POST,'vakuutus',FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        
        $ennakko1 = $brutto / 100 * $ennakko;
        $tyoelake1 = $brutto / 100 * $tyoelake;
        $vakuutus1= $brutto / 100 * $vakuutus; 
        $tulos = $brutto - ($ennakko1 + $tyoelake1 + $vakuutus1);

        printf ("<p>brutto %.2f</p>",$brutto);
        printf ("<p>ennakko %.2f</p>",$ennakko1);
        printf ("<p>tyoelake %.2f</p>",$tyoelake1);
        printf ("<p>vakuutus %.2f</p>",$vakuutus1);
        printf ("<p>netto %.2f</p>",$tulos);

?>
    <a href="index.html">Laske uudestaan</a>
        </div>
</body>
</html>